import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'

import axios from 'axios'
import { State, GettersDefinition } from '../types/store'
import createPersistedState from 'vuex-persistedstate'
import SecureLS from 'secure-ls'
import * as GitLabHelper from '../gitlab'
import log from 'loglevel'

const ls = new SecureLS({ isCompression: false })

Vue.use(Vuex)
Vue.use(VueAxios, axios)

const state: State = {
  token: '',
  userID: '',
  username: '',
  userusername: '',
  gitlabProjectID: '',
  projectName: '',
  issueTemplate: '',
  mergeRequestTemplate: '',
  createdIssue: {},
  mergeRequest: {},
  loading: false,
  loaded: false,
  authInProgress: false,
  validForm: true
}

const getters: GettersDefinition = {
  isAuthenticated: (state: { token: string }) => state.token !== '',
  loading: (state: { authInProgress: boolean }) => state.authInProgress
}

export default new Vuex.Store({
  state: state,
  getters: getters,
  mutations: {
    setUserID (state, userID) {
      state.userID = userID
    },
    setUsername (state, username) {
      state.username = username
    },
    setUserusername (state, userusername) {
      state.userusername = userusername
    },
    setGitlabProjectID (state, gitlabProjectID) {
      state.gitlabProjectID = gitlabProjectID
    },
    setAuthInProgress (state, progress) {
      state.authInProgress = progress
    },
    setToken (state, progress) {
      state.token = progress
    },
    setProjectName (state, projectName) {
      state.projectName = projectName
    },
    setIssueTemplate (state, issueTemplate) {
      state.issueTemplate = issueTemplate
    },
    setMergeRequestTemplate (state, mergeRequestTemplate) {
      state.mergeRequestTemplate = mergeRequestTemplate
    },
    setFormValid (state, valid) {
      state.validForm = valid
    },
    setLoading (state, loading) {
      state.loading = loading
    },
    setLoaded (state, loaded) {
      state.loaded = loaded
    }
  },
  actions: {
    resetState ({ commit }) {
      commit('setGitlabProjectID', '')
      commit('setProjectName', '')
    },
    setGitlabProjectID ({ commit }, gitlabProjectID) {
      commit('setGitlabProjectID', gitlabProjectID)
    },
    setProjectName ({ commit }, projectName) {
      commit('setProjectName', projectName)
    },
    setBlogDescription ({ commit }, blogDescription) {
      commit('setBlogDescription', blogDescription)
    },
    setBlogTags ({ commit }, blogTags) {
      commit('setBlogTags', blogTags)
    },
    setCategory ({ commit }, category) {
      commit('setCategory', category)
    },
    setLoading ({ commit }, loading) {
      commit('setLoading', loading)
    },
    setLoaded ({ commit }, loaded) {
      commit('setLoaded', loaded)
    },
    authenticate ({ commit }, vue: any) {
      commit('setAuthInProgress', true)
      vue.$auth.authenticate('gitlab', {}).then(() => {
        commit('setAuthInProgress', false)
        commit('setToken', vue.$auth.getToken())

        const api = GitLabHelper.getAPI()
        // Get the project ID for www-gitlab-com
        const gitlabComProjectID = GitLabHelper.getGitLabComProjectID()
        commit('setGitlabProjectID', gitlabComProjectID)

        /* Get Issue Templates */
        api.RepositoryFiles
          .showRaw(gitlabComProjectID, '.gitlab/issue_templates/blog-post.md', 'HEAD')
          .then((issueTemplate:any) => {
            commit('setIssueTemplate', issueTemplate)
          })

        /* Get Merge Request Templates */
        api.RepositoryFiles
          .showRaw(gitlabComProjectID, '.gitlab/merge_request_templates/blog-post.md', 'HEAD')
          .then((mergeRequestTemplate:any) => {
            commit('setMergeRequestTemplate', mergeRequestTemplate)
          })

        // Get current user for issue, merge request, and author of blog post
        api.Users.current().then((user:any) => {
          log.debug('API user: ' + user)
          commit('setUserID', user.id)
          commit('setUsername', user.name)
          commit('setUserusername', user.username)
        })
      })
    },
    async jumpStartProject ({ state }) {
      const gitlabComProjectID = GitLabHelper.getGitLabComProjectID()
      const projectName = 'Blog Post: ' + state.projectName

      const fileTitle = state.projectName.replaceAll(' ', '-').toLowerCase()
      // const fileDate = today.toISOString().split('T')[0]

      // Create Issue
      // try {
      //   await api.Issues.create(
      //     gitlabComProjectID,
      //     {
      //       id: state.gitlabProjectID,
      //       labels: ['blog post creator'],
      //       title: title,
      //       description: state.issueTemplate,
      //       assignee_ids: userID
      //     }
      //   ).then((issue) => {
      //     state.createdIssue = issue
      //   })
      // } catch (err) {
      //   console.log('Issues API Error: ' + err)
      //   state.loading = false
      //   state.loaded = true
      // }
      console.log(projectName)
      state.loading = false
      state.loaded = true
    },
    logout ({ commit }, vue: any) {
      vue.$auth.logout()
      commit('setToken', '')
    },
    setFormValid ({ commit }, valid) {
      commit('setFormValid', valid)
    }
  },
  modules: {
  },
  plugins: [
    createPersistedState({
      paths: ['token', 'user'],
      storage: {
        getItem: (key) => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: (key) => ls.remove(key)
      }
    })
  ]
})
