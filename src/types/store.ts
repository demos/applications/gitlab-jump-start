export interface State {
    token: string,
    userID: string,
    username: string,
    userusername: string,
    gitlabProjectID: string,
    projectName: string,
    issueTemplate: string,
    mergeRequestTemplate: string,
    createdIssue: Record<string, unknown>,
    mergeRequest: Record<string, unknown>,
    loading: boolean,
    loaded: boolean,
    authInProgress: boolean,
    validForm: boolean
}

export interface Getters {
    isAuthenticated: boolean,
    loading: boolean
}

export type GettersDefinition = {
    [P in keyof Getters]: (state: State, getters: Getters) => Getters[P];
}
