# GitLab JumpStart

A form that helps to setup a project with fundamental GitLab basics using best practises

## Prerequisite
The following environment variables has to be defined as CI/CD variables:

- CLIENT_ID: the application ID that use GitLab as an OAuth provider, and applications that you've authorized to use your account. 
[More information](https://docs.gitlab.com/ee/integration/oauth_provider.html)
- GITLAB_COM_PROJECT_ID: The project id of wwww-gitlab-com where the issue and merge request will be created.

- GITLAB_ISSUE_PROJECT_ID: This value is where issues will be created. In this case we used our Project ID 42279898 

![project-id](assets/project-id.png)

## Project setup

### yarn commands
- Install dependencies using `yarn install`
- To compiles and hot-reloads for development `yarn serve`
- To compiles and minifies for production `yarn build`
- To lints and fixes files `yarn lint`

### Local Development

1. Generate an Application in GitLab at the group level.
- Set the Callback URL to  `http://localhost:8080`
- Set Conidential to No
- Select API for Scopes

Once completed it should look something like this:

![gitlab-application](assets/gitlab-application.png)

- Finally Export the Application ID as an environment variable `export CLIENT_ID=your-really-long-application-id-here`

2. Run `yarn install` to install all the dependencies
3. Run `yarn serve` and visit http://localhost:8080/ !

### Production Setup
1. Generate an Application in GitLab at the group level.
- Set the Callback URL to  `https://demos.gitlab.io/applications/gitlab-jumpstart/#/`
- Set Confidential to No
- Select API for Scopes

2. Add a CLIENT_ID as a CICD variable with the Application ID produced above.


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Contributors

- Madou Coulibaly
- Sophia Manicor
- Noah Ing

![contributors](assets/contributors.jpg){width=25%}
